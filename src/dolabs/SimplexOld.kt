package dolabs

object SimplexOld {
    // загальна кількість змінних
    val allVars = 7

    // количество базисных переменных
    val basisVars = 3

    //большое число
    val MAX = java.lang.Double.MAX_VALUE

    fun solve(coefsF:DoubleArray,coefsLimit :Array<DoubleArray>,basisCoefs:DoubleArray,
              basisVars:IntArray,basisSol : DoubleArray): DoubleArray {




        //  double[] min = simplexMin(coefsF, coefsLimit, basisCoefs, basisVars, basisSol);
        //printArray(min);
        return simplexMaxMethod(coefsF, coefsLimit, basisCoefs, basisVars, basisSol)
    }

    /**
     * Симплекс-метод. Підрахунок мінімуму.
     */
    internal fun simplexMinMethod(c: DoubleArray, a: Array<DoubleArray>, cB: DoubleArray, bV: IntArray,
                            bD: DoubleArray): DoubleArray {
        val delta = DoubleArray(allVars)
        val min = DoubleArray(basisVars)

        System.out.println("\n\nПошук мінімума\n\n")
        var i: Int
        var j: Int
        var r = -1
        var s = -1
        val tmpA = arrayOfNulls<DoubleArray>(basisVars)
        val tmpBD = DoubleArray(basisVars)

        var k = 0
        while (true) {
            ++k

            // Рахуєм дельти, виділяєм головний стовпець
            // (стовпець з мінімальною оцінкою)
            var deltaMin = java.lang.Double.POSITIVE_INFINITY
            r = -1
            j = 0
            while (j < allVars) {
                var z = 0.0
                i = 0
                while (i < basisVars) {
                    z += cB[i] * a[i][j]
                    ++i
                }
                delta[j] = c[j] - z
                if (deltaMin > delta[j]) {
                    deltaMin = delta[j]
                    r = j
                }
                ++j
            }

            // Умова виходу (всі оцінки невід'ємні)
            if (deltaMin >= 0 || k > 100) {
                break
            }

            // Визначаємо головний рядок (з мінімальним відношенням)
            // bD[i] / a[i][r])
            var minRow = java.lang.Double.POSITIVE_INFINITY
            s = -1
            i = 0
            while (i < basisVars) {
                min[i] = bD[i] / a[i][r]
                if (min[i] < 0) {
                    min[i] = java.lang.Double.NaN
                }
                if (minRow > min[i]) {
                    minRow = min[i]
                    s = i
                }
                ++i
            }

            System.out.println("Головний стовпець: r = " + r)
            System.out.println("Головний рядок:  s = " + s)
            printTable(c, a, cB, bV, bD, min)

            // Головний елемент (на перетині головного рядка і стовпця)
            val element = a[s][r]

            // Зберігаємо вміст масивів (тому що їх значення в ході
            //обчислення змінюються, але залишаються потрібними для цих обчислень)
            i = 0
            while (i < basisVars) {
                tmpA[i] = DoubleArray(allVars)
                j = 0
                while (j < allVars) {
                    tmpA[i]!![j] = a[i][j]
                    ++j
                }
                tmpBD[i] = bD[i]
                ++i
            }

            // Вносимо змінну в базис,
            // Перераховуємо базисне рішення і коефіцієнтів змінних -

            // - в головному рядку
            bV[s] = r
            cB[s] = c[r]
            j = 0
            while (j < allVars) {
                a[s][j] /= element
                ++j
            }
            bD[s] /= element

            // - в інших рядках
            i = 0
            while (i < basisVars) {
                // головний рядок уже перерахований
                if (i == s) {
                    ++i
                    continue
                }

                // элемент головного стовпця
                val air = tmpA[i]!![r]

                // перерахунок коефіцієнтів
                j = 0
                while (j < allVars) {
                    a[i][j] -= air * tmpA[s]!![j] / element
                    ++j
                }

                // перерахунок базисного рішення
                bD[i] -= air * tmpBD[s] / element
                ++i
            }

            printDelta(delta)
            System.out.println("----------------------------------------------------")
        }

        System.out.println("Головний стовпець: r = " + (r + 1))
        System.out.println("Головний рядок:  s = " + (s + 1))
        printTable(c, a, cB, bV, bD, min)
        printDelta(delta)
        return printDecision(bV, bD)
    }

    /**
     * Симплекс-метод. Підрахунок максимуму.
     */
    internal fun simplexMaxMethod(c: DoubleArray, a: Array<DoubleArray>, cB: DoubleArray, bV: IntArray,
                                  bD: DoubleArray): DoubleArray {
        val delta = DoubleArray(allVars)
        val min = DoubleArray(basisVars)

        System.out.println("\n\nЗадача максимізації фкункції F\n\n")
        var i: Int
        var j: Int
        var r = -1
        var s = -1
        val tmpA = arrayOfNulls<DoubleArray>(basisVars)
        val tmpBD = DoubleArray(basisVars)

        var k = 0
        while (true) {
            ++k

            // Рахуєм дельти, виділяємо головний стовпець
            // (стовпець с максимальною оцінкою)
            var deltaMax = java.lang.Double.NEGATIVE_INFINITY
            r = -1
            j = 0
            while (j < allVars) {
                var z = 0.0
                i = 0
                while (i < basisVars) {
                    z += cB[i] * a[i][j]
                    ++i
                }
                delta[j] = c[j] - z
                if (deltaMax < delta[j]) {
                    deltaMax = delta[j]
                    r = j
                }
                ++j
            }

            //Умова виходу (всі оцінки непозитивні)
            if (deltaMax <= 0 || k > 100) {
                break
            }

            // Визначаємо головний рядок (з мінімальним відношенням
            // bD[i] / a[i][r])
            var minRow = java.lang.Double.POSITIVE_INFINITY
            s = -1
            i = 0
            while (i < basisVars) {
                min[i] = bD[i] / a[i][r]
                if (min[i] < 0) {
                    min[i] = java.lang.Double.NaN
                }
                if (minRow > min[i]) {
                    minRow = min[i]
                    s = i
                }
                ++i
            }

            System.out.println("Головний стовпець: r = " + (r + 1))
            System.out.println("Головний рядок:    s = " + (s + 1))
            System.out.println("Головний елемент:  e = " + round(a[s][r], 2))

            printTable(c, a, cB, bV, bD, min)

            //Головний елемент (на перетині головного рядка і стовпця)
            val element = a[s][r]

            // Зберігаємо вміст масивів (тому що їх значення в ході
            // обчислення змінюються, але залишаються потрібними для цих обчислень)
            i = 0
            while (i < basisVars) {
                tmpA[i] = DoubleArray(allVars)
                j = 0
                while (j < allVars) {
                    tmpA[i]!![j] = a[i][j]
                    ++j
                }
                tmpBD[i] = bD[i]
                ++i
            }

            // Вносимо змінну в базис,
            // Перераховуємо базисне рішення і цоефіцієнтів змінних -

            // - в головному рядку
            bV[s] = r
            cB[s] = c[r]
            j = 0
            while (j < allVars) {
                a[s][j] /= element
                ++j
            }
            bD[s] /= element

            // - в інших рядках
            i = 0
            while (i < basisVars) {
                // головний рядок вже перераховано
                if (i == s) {
                    ++i
                    continue
                }

                // элемент головного стовпця
                val air = tmpA[i]!![r]

                // перераховуєм коефіцієнти
                j = 0
                while (j < allVars) {
                    a[i][j] -= air * tmpA[s]!![j] / element
                    ++j
                }

                // перерахунок базисного рішення
                bD[i] -= air * tmpBD[s] / element
                ++i
            }

            printDelta(delta)
            System.out.println("----------------------------------------------------")
        }

        System.out.println("Головний стовпець: r = " + (r + 1))
        System.out.println("Головний рядок:    s = " + (s + 1))
        printTable(c, a, cB, bV, bD, min)
        printDelta(delta)
        return printDecision(bV, bD)
    }

    /**
     * Виводимо рішення
     */
    internal fun printDecision(bV: IntArray, bD: DoubleArray): DoubleArray {
        var i: Int
        var j: Int
        var zm = 0.0
        System.out.println("Всі оцінки невід'ємні, підрахунок завершений.")
        System.out.print("Рішення x = (")
        val res = DoubleArray(basisVars)
        var f: Boolean
        j = 0
        while (j < allVars) {
            f = false
            i = 0
            while (i < basisVars) {
                if (bV[i] == j) {
                    if (j < basisVars) {
                        res[j] = bD[i]
                    }
                    //                    if(i==0){
                    //                     zm=zm+16*bD[i];
                    //                    }
                    //                    if(i==1){
                    //                        zm=zm+12*bD[i];
                    //                    }
                    System.out.print(round(bD[i], 2))
                    f = true
                    break
                }
                ++i
            }
            if (!f) {
                System.out.print("0")
            }
            if (j < allVars - 1) {
                System.out.print(", ")
            }
            ++j
        }
        System.out.println(")")
        zm = 9 * bD[0] + 6 * bD[2] + (4 * 0).toDouble() + (7 * 0).toDouble()
        System.out.println("f = " + round(zm, 2))

        return res
    }

    /**
     * Виводимо рядок оцінок
     */
    internal fun printDelta(delta: DoubleArray) {
        System.out.print("\t\t\t\t")
        for (j in 0 until allVars) {
            System.out.print(round(delta[j], 2) + "\t")
        }
        //  out.println("delta[j]");
        System.out.println()
    }

    /**
     * Виводимо таблицю
     */
    internal fun printTable(c: DoubleArray, a: Array<DoubleArray>, cB: DoubleArray, bV: IntArray,
                            bD: DoubleArray, min: DoubleArray) {
        var i: Int
        var j: Int
        // вивід: рядок коефіцієнтів
        System.out.print("\t\t\t\t")
        j = 0
        while (j < allVars) {
            System.out.print(round(c[j], 2) + "\t")
            ++j
        }
        System.out.println("C[j]")

        // вивід: ряд x
        System.out.print("\t\tbV\tbD\t")
        j = 0
        while (j < allVars) {
            System.out.print("x[" + (j + 1) + "]\t")
            ++j
        }
        System.out.println("bD[i] / a[s][r]")

        i = 0
        while (i < basisVars) {
            System.out.print("\t" /*+ round(cB[i], 2)*/ + "\tx[" + (bV[i] + 1) + "]\t"
                    + round(bD[i], 2) + "\t")
            j = 0
            while (j < allVars) {
                System.out.print(round(a[i][j], 2) + "\t")
                ++j
            }
            System.out.println(round(min[i], 2))
            ++i
        }
    }

    internal fun printArray(ar: DoubleArray) {
        System.out.println()
        for (i in ar.indices) {
            System.out.println("\t[" + i + "] = " + ar[i])
        }
        System.out.println()
    }

    internal fun round(n: Double, p: Int): String {
        if (java.lang.Double.isNaN(n)) {
            return "NaN"
        }
        if (java.lang.Double.isInfinite(n)) {
            return "\u221E"
        }
        val d = Math.pow(10.0, p.toDouble())
        return (Math.round(n * d) / d).toString() + ""
    }
}