/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dolabs

import java.lang.Math.pow
import java.lang.System.out


object Simplex {
    // загальна кількість змінних
    val allVars = 4

    // количество базисных переменных
    val basisVars = 2

    fun solve(coefsF:DoubleArray,
              coefsLimit :Array<DoubleArray>,
              basisCoefs:DoubleArray,
              basisVars:IntArray,
              basisSol : DoubleArray): DoubleArray {

        return simplexMaxMethod(coefsF, coefsLimit, basisCoefs, basisVars, basisSol)
    }

    /**
     * Симплекс-метод. Підрахунок мінімуму.
     */
    internal fun simplexMinMethod(c: DoubleArray, a: Array<DoubleArray>, cB: DoubleArray, bV: IntArray,
                            bS: DoubleArray): DoubleArray {
        val delta = DoubleArray(allVars)
        val min = DoubleArray(basisVars)

        out.println("\n\nПошук мінімума\n\n")
        var i: Int
        var j: Int
        var r = -1
        var s = -1
        val tmpA = arrayOfNulls<DoubleArray>(basisVars)
        val tmpBD = DoubleArray(basisVars)

        var k = 0
        while (true) {
            ++k

            // Рахуєм дельти, виділяєм головний стовпець
            // (стовпець з мінімальною оцінкою)
            var deltaMin = java.lang.Double.POSITIVE_INFINITY
            r = -1
            j = 0
            while (j < allVars) {
                var z = 0.0
                i = 0
                while (i < basisVars) {
                    z += cB[i] * a[i][j]
                    ++i
                }
                delta[j] = c[j] - z
                if (deltaMin > delta[j]) {
                    deltaMin = delta[j]
                    r = j
                }
                ++j
            }

            // Умова виходу (всі оцінки невід'ємні)
            if (deltaMin >= 0 || k > 100) {
                break
            }

            // Визначаємо головний рядок (з мінімальним відношенням)
            // bS[i] / a[i][r])
            var minRow = java.lang.Double.POSITIVE_INFINITY
            s = -1
            i = 0
            while (i < basisVars) {
                min[i] = bS[i] / a[i][r]
                if (min[i] < 0) {
                    min[i] = java.lang.Double.NaN
                }
                if (minRow > min[i]) {
                    minRow = min[i]
                    s = i
                }
                ++i
            }

            out.println("Головний стовпець: r = " + r)
            out.println("Головний рядок:  s = " + s)
            printTable(c, a, cB, bV, bS, min)

            // Головний елемент (на перетині головного рядка і стовпця)
            val element = a[s][r]

            // Зберігаємо вміст масивів (тому що їх значення в ході
            //обчислення змінюються, але залишаються потрібними для цих обчислень)
            i = 0
            while (i < basisVars) {
                tmpA[i] = DoubleArray(allVars)
                j = 0
                while (j < allVars) {
                    tmpA[i]!![j] = a[i][j]
                    ++j
                }
                tmpBD[i] = bS[i]
                ++i
            }

            // Вносимо змінну в базис,
            // Перераховуємо базисне рішення і коефіцієнтів змінних -

            // - в головному рядку
            bV[s] = r
            cB[s] = c[r]
            j = 0
            while (j < allVars) {
                a[s][j] /= element
                ++j
            }
            bS[s] /= element

            // - в інших рядках
            i = 0
            while (i < basisVars) {
                // головний рядок уже перерахований
                if (i == s) {
                    ++i
                    continue
                }

                // элемент головного стовпця
                val air = tmpA[i]!![r]

                // перерахунок коефіцієнтів
                j = 0
                while (j < allVars) {
                    a[i][j] -= air * tmpA[s]!![j] / element
                    ++j
                }

                // перерахунок базисного рішення
                bS[i] -= air * tmpBD[s] / element
                ++i
            }

            printDelta(delta)
            out.println("----------------------------------------------------")
        }

        out.println("Головний стовпець: r = " + (r + 1))
        out.println("Головний рядок:  s = " + (s + 1))
        printTable(c, a, cB, bV, bS, min)
        printDelta(delta)
        return printSolution(bV, bS)
    }

    /**
     * Симплекс-метод. Підрахунок максимуму.
     */

    var fx1 = 0f
    var fx2 = 0f

    fun simplexMaxMethod(cF: DoubleArray, cL: Array<DoubleArray>,
                         cB: DoubleArray, bV: IntArray,
                         bS: DoubleArray): DoubleArray {
        fx1 = bV[1].toFloat()
        fx2 = bV[1].toFloat()

        val delta = DoubleArray(allVars)
        val min = DoubleArray(basisVars)
        System.out.println("\n\nЗадача максимізації функції F=${bV[0]}x1 + ${bV[1]}x2\n\n")

        var i: Int
        var j: Int

        var r: Int
        var s = -1
        
        val tmpA = arrayOfNulls<DoubleArray>(basisVars)
        val tmpBD = DoubleArray(basisVars)

        var k = 0
        while (true) {
            ++k
            // Рахуєм дельти, виділяємо головний стовпець
            // (стовпець с максимальною оцінкою)
            var deltaMax = java.lang.Double.NEGATIVE_INFINITY
            r = -1
            j = 0
            while (j < allVars) {
                var z = 0.0
                i = 0
                while (i < basisVars) {
                    z += cB[i] * cL[i][j]
                    ++i
                }
                delta[j] = cF[j] - z
                if (deltaMax < delta[j]) {
                    deltaMax = delta[j]
                    r = j
                }
                ++j
            }

            //Умова виходу (всі оцінки непозитивні)
            if (deltaMax <= 0 || k > 100) {
                break
            }

            // Визначаємо головний рядок (з мінімальним відношенням
            // bS[i] / cL[i][r])
            var minRow = java.lang.Double.POSITIVE_INFINITY
            s = -1
            i = 0
            while (i < basisVars) {
                min[i] = bS[i] / cL[i][r]
                if (min[i] < 0) {
                    min[i] = java.lang.Double.NaN
                }
                if (minRow > min[i]) {
                    minRow = min[i]
                    s = i
                }
                ++i
            }

            out.println("Головний стовпець  r = " + (r + 1))
            out.println("Головний рядок     s = " + (s + 1))
            out.println("Головний елемент Xrs = " + round(cL[s][r], 2))

            printTable(cF, cL, cB, bV, bS, min)

            //Головний елемент (на перетині головного рядка і стовпця)
            val element = cL[s][r]

            // Зберігаємо вміст масивів (тому що їх значення в ході
            // обчислення змінюються, але залишаються потрібними для цих обчислень)
            i = 0
            while (i < basisVars) {
                tmpA[i] = DoubleArray(allVars)
                j = 0
                while (j < allVars) {
                    tmpA[i]!![j] = cL[i][j]
                    ++j
                }
                tmpBD[i] = bS[i]
                ++i
            }

            // Заносимо змінну в базис,
            // Перераховуємо базисне рішення і коефіцієнтів змінних -
            // - в головному рядку
            
            bV[s] = r
            cB[s] = cF[r]
            j = 0
            while (j < allVars) {
                cL[s][j] /= element
                ++j
            }
            bS[s] /= element

            // - в інших рядках
            i = 0
            while (i < basisVars) {
                // головний рядок вже перераховано
                if (i == s) {
                    ++i
                    continue
                }

                // элемент головного стовпця
                val a_ir = tmpA[i]!![r]

                // перераховуєм коефіцієнти
                j = 0
                while (j < allVars) {
                    cL[i][j] -= a_ir * tmpA[s]!![j] / element
                    ++j
                }

                // перерахунок базисного рішення
                bS[i] -= a_ir * tmpBD[s] / element
                ++i
            }

            printDelta(delta)
            out.println("________________________________________________________\n")
        }

        out.println("Головний стовпець  r = " + (r + 1))
        out.println("Головний рядок     s = " + (s + 1))
        printTable(cF, cL, cB, bV, bS, min)
        printDelta(delta)
        return printSolution(bV, bS)
    }

    /**
     * Виводимо рішення
     */
    internal fun printSolution(bV: IntArray, basisSol: DoubleArray): DoubleArray {
        var i: Int
        var j: Int = 0
        var zm = 0.0
        out.println("Всі оцінки невід'ємні, обчислення завершено")
        out.print("Розв'язок X = (")
        val res = DoubleArray(basisVars)
        var f: Boolean
        while (j < allVars) {
            f = false
            i = 0
            while (i < basisVars) {
                if (bV[i] == j) {
                    if (j < basisVars) {
                        res[j] = basisSol[i]
                    }
                    //                    if(i==0){
                    //                     zm=zm+16*basisSol[i];
                    //                    }
                    //                    if(i==1){
                    //                        zm=zm+12*basisSol[i];
                    //                    }
                    out.print(round(basisSol[i], 2))
                    f = true
                    break
                }
                ++i
            }
            if (!f) {
                out.print("0")
            }
            if (j < allVars - 1) {
                out.print(", ")
            }
            ++j
        }
        out.println(")")
        zm = fx1 * basisSol[0] + fx2 * basisSol[1] + (4 * 0).toDouble() + (7 * 0).toDouble()
        out.println("F = " + round(zm, 2))

        return res
    }

    /**
     * Виводимо рядок оцінок
     */
    internal fun printDelta(delta: DoubleArray) {
        out.print("\t\t\t\t")
        for (j in 0 until allVars) {
            out.print(round(delta[j], 2) + "\t")
        }
        //  out.println("delta[j]");
        out.println()
    }

    /**
     * Виводимо таблицю
     */
    internal fun printTable(c: DoubleArray, a: Array<DoubleArray>, cB: DoubleArray, bV: IntArray,
                            bS: DoubleArray, min: DoubleArray) {
        var i: Int = 0
        var j: Int
        // вивід: рядок коефіцієнтів
        out.print("\t\t\t\t")
        j = 0
        while (j < allVars) {
            out.print(round(c[j], 2) + "\t")
            ++j
        }
        out.println("c[j]")

        // вивід: ряд x
        out.print("\t\tbV\tbS\t")
        j = 0
        while (j < allVars) {
            out.print("x[" + (j + 1) + "]\t")
            ++j
        }
        out.println("bS[i] / a[s][r]")

        while (i < basisVars) {
            out.print("\t" /*+ round(cB[i], 2)*/ + "\tx[" + (bV[i] + 1) + "]\t"
                    + round(bS[i], 2) + "\t")
            j = 0
            while (j < allVars) {
                out.print(round(a[i][j], 2) + "\t")
                ++j
            }
            out.println(round(min[i], 2))
            ++i
        }
    }

    internal fun printArray(ar: DoubleArray) {
        out.println()
        for (i in ar.indices) {
            out.println("\t[" + i + "] = " + ar[i])
        }
        out.println()
    }

    internal fun round(n: Double, p: Int): String {
        if (java.lang.Double.isNaN(n)) {
            return "NaN"
        }
        if (java.lang.Double.isInfinite(n)) {
            return "\u221E"
        }
        val d = pow(10.0, p.toDouble())
        return (Math.round(n * d) / d).toString() + ""
    }
}
