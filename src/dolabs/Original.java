/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dolabs;

import static java.lang.Math.pow;
import static java.lang.System.out;

/**
 *
 * @author Ol'ko
 */
public class Original {
 // загальна кількість змінних
    final static int n = 7;
    
    // количество базисных переменных   
    final static int m = 3;
    
    //большое число
    final static double M = Double.MAX_VALUE;
 
    public static void main() {
        // коэффициенты из целевой функции
        // коэффициенты из ограничений
        // коэффициенты при базисных переменных
        // базисные переменные
        // базисное решениее переменные
 
        double[] c = new double[n];
        double[][] a = new double[m][n];
        double[] cB = new double[m];
        int[] bV = new int[m];
        double[] bD = new double[m];
 
        c[0] = 9;
        c[1] = 6;
        c[2] = 4;
        c[3] = 7;
        c[4] = 0;
        c[5] = 0;
        c[6] = 0;
 
        a[0][0] = 1; a[0][1] =0; a[0][2] = 2; a[0][3] = 1; a[0][4] = 1; a[0][5] = 0; a[0][6] = 0;
        a[1][0] = 0; a[1][1] =1; a[1][2] = 3; a[1][3] = 2; a[1][4] = 0; a[1][5] = 1; a[1][6] = 0;
        a[2][0] = 4; a[2][1] =2; a[2][2] = 0; a[2][3] = 4; a[2][4] = 0; a[2][5] = 0; a[2][6] = 1;
 
        cB[0] = 0;
        cB[1] = 0;
        cB[2] = 0;
 
        bV[0] = 4;
        bV[1] = 5;
        bV[2] = 6;
 
        bD[0] = 180;
        bD[1] = 210;
        bD[2] = 800;
 
      //  double[] min = simplexMin(c, a, cB, bV, bD);
        //printArray(min);
        
        double[] max = simplexMax(c, a, cB, bV, bD);
        //printArray(max);
    }
 
    /**
     * Симплекс-метод. Підрахунок мінімуму.
     */
    static double[] simplexMin(double[] c, double[][] a, double[] cB, int[] bV,
            double[] bD) {
        double[] delta = new double[n];
        double[] min = new double[m];
 
        out.println("\n\nПошук мінімума\n\n");
        int i, j, r = -1, s = -1;
        double[][] tmpA = new double[m][];
        double[] tmpBD = new double[m];
 
        int k = 0;
        while (true) {
            ++k;
 
            // Рахуєм дельти, виділяєм головний стовпець
            // (стовпець з мінімальною оцінкою)
            double deltaMin = Double.POSITIVE_INFINITY;
            r = -1;
            for (j = 0; j < n; ++j) {
                double z = 0;
                for (i = 0; i < m; ++i) {
                    z += cB[i] * a[i][j];
                }
                delta[j] = c[j] - z;
                if (deltaMin > delta[j]) {
                    deltaMin = delta[j];
                    r = j;
                }
            }
 
            // Умова виходу (всі оцінки невід'ємні)
            if (deltaMin >= 0 || k > 100) {
                break;
            }
 
            // Визначаємо головний рядок (з мінімальним відношенням)
            // bD[i] / a[i][r])
            double minRow = Double.POSITIVE_INFINITY;
            s = -1;
            for (i = 0; i < m; ++i) {
                min[i] = bD[i] / a[i][r];
                if (min[i] < 0) {
                    min[i] = Double.NaN;
                }
                if (minRow > min[i]) {
                    minRow = min[i];
                    s = i;
                }
            }
 
            out.println("Головний стовпець: r = " + r);
            out.println("Головний рядок:  s = " + s);
            printTable(c, a, cB, bV, bD, min);
 
            // Головний елемент (на перетині головного рядка і стовпця)
            double element = a[s][r];
 
            // Зберігаємо вміст масивів (тому що їх значення в ході
            //обчислення змінюються, але залишаються потрібними для цих обчислень)
            for (i = 0; i < m; ++i) {
                tmpA[i] = new double[n];
                for (j = 0; j < n; ++j) {
                    tmpA[i][j] = a[i][j];
                }
                tmpBD[i] = bD[i];
            }
 
            // Вносимо змінну в базис,
            // Перераховуємо базисне рішення і коефіцієнтів змінних -
 
            // - в головному рядку
            bV[s] = r;
            cB[s] = c[r];
            for (j = 0; j < n; ++j) {
                a[s][j] /= element;
            }
            bD[s] /= element;
 
            // - в інших рядках
            for (i = 0; i < m; ++i) {
                // головний рядок уже перерахований
                if (i == s) {
                    continue;
                }
 
                // элемент головного стовпця
                double air = tmpA[i][r];
 
                // перерахунок коефіцієнтів
                for (j = 0; j < n; ++j) {
                    a[i][j] -= (air * tmpA[s][j]) / element;
                }
 
                // перерахунок базисного рішення
                bD[i] -= (air * tmpBD[s]) / element;
            }
 
            printDelta(delta);
            out.println("----------------------------------------------------");
        }
 
        out.println("Головний стовпець: r = " + (r + 1));
        out.println("Головний рядок:  s = " + (s + 1));
        printTable(c, a, cB, bV, bD, min);
        printDelta(delta);
        return printDecision(bV, bD);
    }
 
    /**
     *Симплекс-метод. Підрахунок максимуму.
     */
    static double[] simplexMax(double[] c, double[][] a, double[] cB, int[] bV,
            double[] bD) {
        double[] delta = new double[n];
        double[] min = new double[m];
 
        out.println("\n\nПошук максимума\n\n");
        int i, j, r = -1, s = -1;
        double[][] tmpA = new double[m][];
        double[] tmpBD = new double[m];
 
        int k = 0;
        while (true) {
            ++k;
 
            // Рахуєм дельти, виділяємо головний стовпець
            // (стовпець с максимальною оцінкою)
            double deltaMax = Double.NEGATIVE_INFINITY;
            r = -1;
            for (j = 0; j < n; ++j) {
                double z = 0;
                for (i = 0; i < m; ++i) {
                    z += cB[i] * a[i][j];
                }
                delta[j] = c[j] - z;
                if (deltaMax < delta[j]) {
                    deltaMax = delta[j];
                    r = j;
                }
            }
 
            //Умова виходу (всі оцінки непозитивні)
            if (deltaMax <= 0 || k > 100) {
                break;
            }
 
            // Визначаємо головний рядок (з мінімальним відношенням
            // bD[i] / a[i][r])
            double minRow = Double.POSITIVE_INFINITY;
            s = -1;
            for (i = 0; i < m; ++i) {
                min[i] = bD[i] / a[i][r];
                if (min[i] < 0) {
                    min[i] = Double.NaN;
                }
                if (minRow > min[i]) {
                    minRow = min[i];
                    s = i;
                }
            }
 
            out.println("Головний стовпець: r = " + (r+1));
            out.println("Головний рядок:    s = " + (s+1));
            out.println("Головний елемент:  e = " +round(a[s][r],2));

            printTable(c, a, cB, bV, bD, min);
 
            //Головний елемент (на перетині головного рядка і стовпця)
            double element = a[s][r];

            // Зберігаємо вміст масивів (тому що їх значення в ході
            // обчислення змінюються, але залишаються потрібними для цих обчислень)
            for (i = 0; i < m; ++i) {
                tmpA[i] = new double[n];
                for (j = 0; j < n; ++j) {
                    tmpA[i][j] = a[i][j];
                }
                tmpBD[i] = bD[i];
            }
 
            // Вносимо змінну в базис,
            // Перераховуємо базисне рішення і цоефіцієнтів змінних -
 
            // - в головному рядку
            bV[s] = r;
            cB[s] = c[r];
            for (j = 0; j < n; ++j) {
                a[s][j] /= element;
            }
            bD[s] /= element;
 
            // - в інших рядках
            for (i = 0; i < m; ++i) {
                // головний рядок вже перераховано
                if (i == s) {
                    continue;
                }
 
                // элемент головного стовпця
                double air = tmpA[i][r];
 
                // перераховуєм коефіцієнти
                for (j = 0; j < n; ++j) {
                    a[i][j] -= (air * tmpA[s][j]) / element;
                }
 
                // перерахунок базисного рішення
                bD[i] -= (air * tmpBD[s]) / element;
            }
 
            printDelta(delta);
            out.println("----------------------------------------------------");
        }
 
        out.println("Головний стовпець: r = " + (r + 1));
        out.println("Головний рядок:    s = " + (s + 1));
        printTable(c, a, cB, bV, bD, min);
        printDelta(delta);
        return printDecision(bV, bD);
    }
 
    /**
     * Виводимо рішення
     */
    static double[] printDecision(int[] bV, double[] bD) {
        int i, j;
        double zm=0;
        out.println("Всі оцінки невід'ємні, підрахунок завершений.");
        out.print("Рішення x = (");
        double[] res = new double[m];
        boolean f;
        for (j = 0; j < n; ++j) {
            f = false;
            for (i = 0; i < m; ++i) {
                if (bV[i] == j) {
                    if (j < m) {
                        res[j] = bD[i];
                    }
//                    if(i==0){
//                     zm=zm+16*bD[i];   
//                    }
//                    if(i==1){
//                        zm=zm+12*bD[i];
//                    }
                    out.print(round(bD[i], 2));
                    f = true;
                    break;
                }
            }
            if (!f) {
                out.print("0");
            }
            if (j < n - 1) {
                out.print(", ");
            }
        }
        out.println(")");
        zm=(9*bD[0]+6*bD[2]+4*0+7*0);
                    out.println("f = "+round(zm, 2));

        return res;
    }
 
    /**
     * Виводимо рядок оцінок
     */
    static void printDelta(double[] delta) {
        out.print("\t\t\t\t");
        for (int j = 0; j < n; ++j) {
            out.print(round(delta[j], 2) + "\t");
        }
      //  out.println("delta[j]");
        out.println();
    }
 
    /**
     * Виводимо таблицю
     */
    static void printTable(double[] c, double[][] a, double[] cB, int[] bV,
            double[] bD, double[] min) {
        int i, j;
        // вивід: рядок коефіцієнтів
        out.print("\t\t\t\t");
        for (j = 0; j < n; ++j) {
            out.print(round(c[j], 2) + "\t");
        }
        out.println("C[j]");
 
        // вивід: ряд x
        out.print("\t\tbV\tbD\t");
        for (j = 0; j < n; ++j) {
            out.print("x[" + (j+1) + "]\t");
        }
        out.println("bD[i] / a[s][r]");
 
        for (i = 0; i < m; ++i) {
            out.print("\t" /*+ round(cB[i], 2)*/ + "\tx[" + (bV[i] + 1) + "]\t"
                    + round(bD[i], 2) + "\t");
            for (j = 0; j < n; ++j) {
                out.print(round(a[i][j], 2) + "\t");
            }
            out.println(round(min[i], 2));
        }
    }
    
    static void printArray(double[] ar)
    {
        out.println();
        for (int i = 0; i < ar.length; ++i) {
            out.println("\t[" + i + "] = " + ar[i]);
        }
        out.println();
    }
 
    static String round(double n, int p) {
        if (Double.isNaN(n)) {
            return "NaN";
        }
        if (Double.isInfinite(n)) {
            return "\u221E";
        }
        double d = pow(10, p);
        return (Math.round(n * d) / d) + "";
    }
}
