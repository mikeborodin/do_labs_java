package dolabs;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class SimplexController {

    public SimplexController(){}

    @FXML
    TextArea out;

    @FXML
    TextField x11;

    @FXML
    TextField x12;

    @FXML
    TextField b1;

    @FXML
    TextField x21;

    @FXML
    TextField x22;

    @FXML
    TextField b2;

    @FXML
    TextField fx1;

    @FXML
    TextField fx2;




    public void clear(){
        out.clear();
    }

    public void printOutput(ActionEvent actionEvent) {


        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        // IMPORTANT: Save the old System.out!
        PrintStream old = System.out;
        // Tell Java to use your special stream
        System.setOut(ps);
        // Print some output: goes to your special stream
        // Put things back
        solve();
        System.out.flush();
        System.setOut(old);
        // Show what happened
        System.out.println(baos.toString());

        out.setText(baos.toString());


    }

    private void solve() {

        //initialization

        // коэффициенты из целевой функции
        double[] coefsF = new double[Simplex.INSTANCE.getAllVars()];

        // коэффициенты из ограничений
        double[][] coefsLimit = new double[Simplex.INSTANCE.getBasisVars()][];
        for (int i = 0; i < coefsLimit.length; i++) {
            coefsLimit[i] =  new double[Simplex.INSTANCE.getAllVars()];
        }

        // коэффициенты при базисных переменных
        double[] basisCoefs = new double[Simplex.INSTANCE.getBasisVars()];

        // базисные переменные
        int[] basisVars = new int[Simplex.INSTANCE.getBasisVars()];

        // базисное решениее переменные
        double[] basisSol = new double[Simplex.INSTANCE.getBasisVars()];



        //set values
        float _x11 = Float.valueOf(x11.getText());
        float _x12 = Float.valueOf(x12.getText());
        float _b1 = Float.valueOf(b1.getText());

        float _x21 = Float.valueOf(x21.getText());
        float _x22 = Float.valueOf(x22.getText());
        float _b2 = Float.valueOf(b2.getText());
        float _fx1 = Float.valueOf(fx1.getText());
        float _fx2 = Float.valueOf(fx2.getText());


        if(_fx1<0||_fx2<0 || _b1<0 || _b2<0){
            System.out.println("Тільки невід'ємні значення допускаються. Перевірте вхідні дані, будь  ласка.");
            return;
        }

        coefsF[0] = _b1;
        coefsF[1] = _b2;

        coefsLimit[0][0] = _x11;
        coefsLimit[0][1] = _x12;
        coefsLimit[0][2] = 1.0;
        coefsLimit[0][3] = 0.0;


        coefsLimit[1][0] = _x21;
        coefsLimit[1][1] = _x22;
        coefsLimit[1][2] = 0.0;
        coefsLimit[1][3] = 1.0;

        basisCoefs[0] = 0.0;
        basisCoefs[1] = 0.0;

        basisVars[0] = (int) _fx1;
        basisVars[1] = (int) _fx2;

        basisSol[0] = _b1;
        basisSol[1] = _b2;



        //Original.main();
        Simplex.INSTANCE.solve(coefsF, coefsLimit, basisCoefs, basisVars, basisSol);
    }



    /*private void solve() {


        Float _x11 = Float.valueOf(x11.getText());
        Float _x12 = Float.valueOf(x12.getText());
        Float _b1 = Float.valueOf(b1.getText());

        Float _x21 = Float.valueOf(x21.getText());
        Float _x22 = Float.valueOf(x22.getText());
        Float _b2 = Float.valueOf(b2.getText());

        float[] values = new float[]{
                _x11,_x12,_b1,_x21,_x22,_b2};

        // коэффициенты из целевой функции
        double[] coefsF = new double[Simplex.INSTANCE.getAllVars()];

        // коэффициенты из ограничений
        double[][] coefsLimit = new double[Simplex.INSTANCE.getBasisVars()][];
        for (int i = 0; i < coefsLimit.length; i++) {
            coefsLimit[i] =  new double[Simplex.INSTANCE.getAllVars()];
        }

        // коэффициенты при базисных переменных
        double[] basisCoefs = new double[Simplex.INSTANCE.getBasisVars()];

        // базисные переменные
        int[] basisVars = new int[Simplex.INSTANCE.getBasisVars()];

        // базисное решениее переменные
        double[] basisSol = new double[Simplex.INSTANCE.getBasisVars()];



        //set values


        coefsF[0] = 9.0;
        coefsF[1] = 6.0;
        coefsF[2] = 4.0;
        coefsF[3] = 7.0;
        coefsF[4] = 0.0;
        coefsF[5] = 0.0;
        coefsF[6] = 0.0;

        coefsLimit[0][0] = 1.0;
        coefsLimit[0][1] = 0.0;
        coefsLimit[0][2] = 2.0;
        coefsLimit[0][3] = 1.0;
        coefsLimit[0][4] = 1.0;
        coefsLimit[0][5] = 0.0;
        coefsLimit[0][6] = 0.0;

        coefsLimit[1][0] = 0.0;
        coefsLimit[1][1] = 1.0;
        coefsLimit[1][2] = 3.0;
        coefsLimit[1][3] = 2.0;
        coefsLimit[1][4] = 0.0;
        coefsLimit[1][5] = 1.0;
        coefsLimit[1][6] = 0.0;

        coefsLimit[2][0] = 4.0;
        coefsLimit[2][1] = 2.0;
        coefsLimit[2][2] = 0.0;
        coefsLimit[2][3] = 4.0;
        coefsLimit[2][4] = 0.0;
        coefsLimit[2][5] = 0.0;
        coefsLimit[2][6] = 1.0;

        basisCoefs[0] = 0.0;
        basisCoefs[1] = 0.0;
        basisCoefs[2] = 0.0;

        basisVars[0] = 4;
        basisVars[1] = 5;
        basisVars[2] = 6;

        basisSol[0] = 180.0;
        basisSol[1] = 210.0;
        basisSol[2] = 800.0;

        //Original.main();
        Simplex.INSTANCE.solve(coefsF, coefsLimit, basisCoefs, basisVars, basisSol);
    }
*/
}
